module.exports = {
  mysql: {
    connectionLimit: process.env.MYSQL_CONN_LIMIT,
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB,
    debug: false,
    timezone: 'UTC',
  },
  MAIL_ADDRESSES_RECIPIENT:
    process.env.NODE_ENV == 'prod'
      ? process.env.MAIL_ADDRESSES_RECIPIENT_PROD
      : process.env.MAIL_ADDRESSES_RECIPIENT_TEST,
};
