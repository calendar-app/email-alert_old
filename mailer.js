/* eslint-disable max-len */
const HtmlCreator = require('html-creator');
const moment = require('moment');
moment.locale('it');
const mailSender = require('./infrastucture/mailSender');

/**
 * Create html email.
 * @param {Array} values .
 * @return {string} .
 */
function createMail([overlapped, missingValue]) {
  // MAIN BODY
  const html = new HtmlCreator([{
    type: 'body',
    content: [{
      type: 'div',
      content: [{
        type: 'div',
        attributes: { style: 'text-align: center' },
        content: [
          {
            type: 'img',
            attributes: { alt: '', height: 50, width: 50, src: 'cid:cal100' },
          },
        ],
      }, {
        type: 'div',
        attributes: { id: 'overlap' },
        content: [],
      }, {
        type: 'div',
        attributes: { id: 'missing' },
        content: [],
      }, {
        type: 'div',
        content: [{
          type: 'em',
          content: 'email generata automaticamente',
        }, {
          type: 'div',
          content: [
            {
              type: 'span',
              content: 'Powered by ',
            }, {
              type: 'em',
              content: 'ITS Calendar',
            },
          ],
        }],
      }],
    }],
  }]);

  if (overlapped.length > 0) {
    // OVERLAP VALUE - HEADER
    html.document.addElementToId('overlap', {
      type: 'div',
      content: [{
        type: 'h3',
        attributes: { style: 'text-align: center' },
        content: `Avviso Possibili <b>ORARI SOVRAPPOSTI</b> (${overlapped.length / 2})`
          + `<br>Controllare le seguenti coppie di orari: `,
      }, {
        type: 'ul',
        attributes: { id: 'overlap-ul' },
        content: {},
      }],
    });

    // OVERLAP VALUE - ELEMENTS
    overlapped.forEach((item) => {
      const start = item.timestart.split(':');
      const end = item.timeend.split(':');

      html.document.addElementToId('overlap-ul', {
        type: 'li',
        attributes: { style: 'margin-top: 20px' },
        content: [{
          type: 'div',
          attributes: { style: 'font-weight: bold' },
          content: item.course,
        }, {
          type: 'div',
          attributes: { style: 'font-weight: bold' },
          content: item.module,
        }, {
          type: 'div',
          content: `Prof: ${item.prof}`,
        }, {
          type: 'div',
          content: `Data: ${moment(item.date).format('ddd D MMM YY').toLowerCase()}`,
        }, {
          type: 'div',
          content: `Orario: ${start[0]}${parseInt(start[1]) ? ':' + start[1] : ''} - ${end[0]}${parseInt(end[1]) ? ':' + end[1] : ''}`,
        }],
      });
    });
  }

  if (missingValue.length > 0) {
    // MISSING VALUE - HEADER
    html.document.addElementToId('overlap', {
      type: 'div',
      content: [{
        type: 'h3',
        attributes: { style: 'text-align: center' },
        content: `Avviso Dati Mancanti (${missingValue.length})`
          + '<br>Controllare la prenotazione delle aule dei seguenti orari:',
      }, {
        type: 'ul',
        attributes: { id: 'missing-ul' },
        content: {},
      }],
    });

    // MISSING VALUE - ELEMENTS
    missingValue.forEach((item) => {
      const start = item.timestart.split(':');
      const end = item.timeend.split(':');

      html.document.addElementToId('missing-ul', {
        type: 'li',
        attributes: { style: 'margin-top: 20px' },
        content: [{
          type: 'div',
          attributes: { style: 'font-weight: bold' },
          content: item.course,
        }, {
          type: 'div',
          attributes: { style: 'font-weight: bold' },
          content: item.module,
        }, {
          type: 'div',
          content: `Prof: ${item.prof}`,
        }, {
          type: 'div',
          content: `Data: ${moment(item.date).format('ddd D MMM YY').toLowerCase()}`,
        }, {
          type: 'div',
          content: `Orario: ${start[0]}${parseInt(start[1]) ? ':' + start[1] : ''} - ${end[0]}${parseInt(end[1]) ? ':' + end[1] : ''}`,
        }],
      });
    });
  }

  // html.renderHTMLToFile('./index.html');

  return html.renderHTML();
}

/**
 * Send email with given options.
 * @param {object} mailOptions .
 */
function send(mailOptions) {
  mailSender.send(mailOptions);
}

module.exports = {
  createMail,
  send,
};
