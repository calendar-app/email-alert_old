const mysql = require('mysql');

/**
 * @class
 */
class Database {
  /**
   * @param {object} config .
   */
  constructor(config) {
    this.connection = mysql.createConnection(config);
  }

  /**
   * Execute query with given params
   * @param {string} sql query.
   * @param {Array} args params array.
   * @return {Promise} .
   */
  query(sql, args) {
    return new Promise((resolve, reject) => {
      this.connection.query(sql, args, (err, rows) => {
        if (err) {
          this.connection.end();
          return reject(err);
        }
        resolve(rows);
        this.connection.end();
      });
    });
  }
}

module.exports.Database = Database;
