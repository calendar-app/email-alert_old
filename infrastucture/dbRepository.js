const config = require('../config');
const mydb = require('./dbConnection');

/**
 * Get Times array with date >= today.
 * @return {Array} RowDataPacket Array.
 */
async function getTimesEqualOrAfterToday() {
  const db = new mydb.Database(config.mysql);
  return await db.query(`
        SELECT *
        FROM Times
        WHERE date >= CURDATE()
        ORDER BY id_course;`);
}

/**
 * Get Times array by given ids.
 * @param {Array} timesIds Ids list.
 * @return {Array} RowDataPacket Array.
 */
async function getTimeEventsByIdList(timesIds) {
  const db = new mydb.Database(config.mysql);
  return await db.query(`
            SELECT 
                Times.id         id,
                Course.name      course,
                Course.csvCode   code,
                Times.moduleName module,
                Professor.name   prof,
                Times.date       date,
                Times.timestart  timestart,
                Times.timeend    timeend
            FROM Calendar.Course
                JOIN Calendar.Times 
                    on Course.id = Times.id_course
                JOIN Calendar.Professor
                    on Times.id_professor = Professor.id
            WHERE Times.id IN (${timesIds.join(', ')})
            ORDER BY prof;`);
}

/**
 * Get Times array with null value for room field.
 * @return {Array} RowDataPacket Array.
 */
async function getTimesWithNullRoom() {
  const db = new mydb.Database(config.mysql);
  return await db.query(`
        SELECT 
            Times.id         id,
            Course.name      course,
            Course.csvCode   code,
            Times.moduleName module,
            Professor.name   prof,
            Times.date       date,
            Times.timestart  timestart,
            Times.timeend    timeend
        FROM Calendar.Course
            JOIN Calendar.Times on Course.id = Times.id_course
            JOIN Calendar.Professor on Times.id_professor = Professor.id
        WHERE Times.id_room IS NULL
        AND Times.date >= CURDATE()
        ORDER BY Course.name;`);
}

module.exports = {
  getTimesEqualOrAfterToday,
  getTimeEventsByIdList,
  getTimesWithNullRoom,
};
