/* eslint-disable max-len */
const nodemailer = require('nodemailer');

/**
 * Send Mail(s) with given options.
 * @param {object} mailOptions options for email.
 */
function send(mailOptions) {
  const transporter = nodemailer.createTransport({
    service: process.env.NODEMAILER_SERVICE,
    auth: {
      user: process.env.NODEMAILER_MAIL,
      pass: process.env.NODEMAILER_PASS,
    },
  });

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) console.log(err);
    else console.log('Email sent: ' + info.response + '\n' + info.messageId);
  });

  // await db.query(`INSERT INTO log (action, times) VALUES (?, ?)`, [e ? 'mailERROR' : 'mailSEND', warning]);
}

module.exports = {
  send,
};
