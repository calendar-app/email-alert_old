/* eslint-disable max-len */

// Import environments
require('dotenv').config();
const cron = require('node-cron');
const moment = require('moment');
moment.locale('it');

const checker = require('./checker');
const mailer = require('./mailer');
const config = require('./config');

/**
 * Execute check and send email if there are some problems.
 */
function checkAndSend() {
  console.log(`\n[${moment().format('DD/MM/YYYY HH:mm:ss')}]\tStart checking... `);

  Promise.all([
    checker.checkOverlap(),
    checker.checkMissingValue(),
  ])
      .then(([overlapped, missingValue]) => {
        if (overlapped.length > 0 || missingValue.length > 0) {
          const mailBody = mailer.createMail([overlapped, missingValue]);

          const mailOptions = {
            to: config.MAIL_ADDRESSES_RECIPIENT,
            subject: `Dati Mancanti - ${moment().format('DD/MM/YYYY')} - ITS Calendar`,
            html: mailBody,
            attachments: [{
              filename: 'cal-100.png',
              path: './cal-100.png',
              cid: 'cal100', // same cid value as in the html img src
            }],
          };

          mailer.send(mailOptions);

          // TODO upload log
          // await db.query(`INSERT INTO log (action, times) VALUES (?, ?)`, [e ? 'mailERROR' : 'mailSEND', warning]);
        } else {
          console.log('Everything OK');
        }
      })
      .catch(console.err);
}

console.log(`\n[${moment().format('DD/MM/YYYY HH:mm:ss')}] MAILER STARTED\n`);
cron.schedule(process.env.CRON_SCHEDULE_FIRST, checkAndSend);
console.log(`Cron started! [${process.env.CRON_SCHEDULE_FIRST}]`);

cron.schedule(process.env.CRON_SCHEDULE_SECOND, checkAndSend);
console.log(`Cron started! [${process.env.CRON_SCHEDULE_SECOND}]`);

checkAndSend();
