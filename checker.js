/* eslint-disable prefer-const */
/* eslint-disable max-len */
const moment = require('moment');
moment.locale('it');

const dbRepository = require('./infrastucture/dbRepository');

/**
 * Check when there are overlapped times.
 * @return {Promise} .
 */
function checkOverlap() {
  return new Promise(async (resolve, reject) => {
    try {
      const times = await dbRepository.getTimesEqualOrAfterToday();

      let overlapTimesIds = [];
      times.forEach((quizItem) => {
        times.forEach((item) => {
          if (item.id != quizItem.id // check only item with different id
            && moment(item.date).isSame(moment(quizItem.date))
            && moment(item.timestart, 'HH:mm:ss').isSame(moment(quizItem.timestart, 'HH:mm:ss'))
            && moment(item.timeend, 'HH:mm:ss').isSame(moment(quizItem.timeend, 'HH:mm:ss'))
            && parseInt(item.id_professor) == parseInt(quizItem.id_professor)
            && !overlapTimesIds.includes(item.id)
            && !overlapTimesIds.includes(quizItem.id)) {
            overlapTimesIds.push(quizItem.id);
            overlapTimesIds.push(item.id);
          }
        });
      });

      if (overlapTimesIds.length) {
        const overlapTimeEvents = await dbRepository.getTimeEventsByIdList(overlapTimesIds);
        return resolve(overlapTimeEvents);
      }

      resolve([]);
    } catch (ex) {
      reject(ex);
    }
  });
}

/**
 * Check when there are null room fiels for times.
 * @return {Promise} .
 */
function checkMissingValue() {
  return new Promise(async (resolve, reject) => {
    try {
      const timesWithNullRoom = await dbRepository.getTimesWithNullRoom();
      if (timesWithNullRoom.length > 0) {
        return resolve(timesWithNullRoom);
      }

      resolve([]);
    } catch (ex) {
      reject(ex);
    }
  });
}

module.exports = {
  checkOverlap,
  checkMissingValue,
};
